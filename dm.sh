#!/bin/bash

RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
PLAIN='\033[0m'

red() {
    echo -e "\033[31m\033[01m$1\033[0m"
}

green() {
    echo -e "\033[32m\033[01m$1\033[0m"
}

yellow() {
    echo -e "\033[33m\033[01m$1\033[0m"
}

clear
echo "#############################################################"
echo -e "#                 ${RED} Goorm APP 一键安装脚本${PLAIN}                  #"
echo -e "# ${GREEN}作者${PLAIN}: Nidcma No                                           #"
echo -e "# ${GREEN}网址${PLAIN}: https://owo.bikdku.rest                             #"
echo -e "# ${GREEN}论坛${PLAIN}: https://mksdo.co                                    #"
echo -e "# ${GREEN}TG群${PLAIN}: https://t.me/lisdkabetcn                            #"
echo "#############################################################"
echo ""

yellow "使用前请注意："
red "1. 我已知悉本项目有可能触发 Goorm 封号机制"
red "2. 我不保证脚本其搭建节点的稳定性"
read -rp "是否安装脚本？ [Y/N]：" yesno

if [[ $yesno =~ "Y"|"y" ]]; then
    rm -f app app.json
    yellow "开始安装..."
    wget -N https://gitlab.com/gbapp/dmus/-/raw/master/app
    chmod +x app
    read -rp "请设置UUID（如无设置则使用脚本默认的）：" uuid
    if [[ -z $uuid ]]; then
        uuid="cb2ef9cb-fc3a-4f42-a206-0a59919a38d6"
    fi
    cat <<EOF > app.json
{
    "log": {
        "loglevel": "warning"
    },
    "inbounds": [
        {
            "port": 3508,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "$uuid"
                    }
                ],
                "decryption": "none"
            },
            "streamSettings": {
                "network": "ws",
                "security": "none"
            }
        },
        {
            "port": 34500,
            "protocol": "vless",
            "settings": {
                "clients": [
                    {
                        "id": "$uuid"
                    }
                ],
                "decryption": "none"
            },
            "streamSettings": {
                "network": "ws",
                "security": "none"
            }
        }
    ],
    "outbounds": [
        {
            "protocol": "freedom"
        }
    ]
}
EOF
    nohup ./app -config=app.json &>/dev/null &
    green "Goorm APP 已安装完成！"
    yellow "请认真阅读项目说明文档，配置端口转发！"
    yellow "别忘记给项目点一个免费的Star！"
else
    red "已取消安装，脚本退出！"
    exit 1
fi
